@extends('navbar')
@section('content')
  
<div class="section-container">
  <div class="container">
    <div class="row">
      <div class="col-xs-12">
        <img src="./assets/images/work001-01.jpg" class="img-responsive" alt="">
        <div class="card-container">
          <div class="text-center">
            <h1 class="h2">001 : Music and Games</h1>
          </div>
          <p> Games dan Musik adalah suatu elemen yang ada dalah hidup gua </p>

          <blockquote>
            <p>"kerjakan apa yang kamu suka, maka kamu akan mencintai karya mu"</p>
            <small class="pull-right">Yehuda Bezaleel</small>
          </blockquote>
        </div>
      </div>

      
      <div class="col-md-8 col-md-offset-2 section-container-spacer">
        <div class="row">
          <div class="col-xs-12 col-md-6">
            <img src="./assets/images/music.jpg" class="img-responsive" alt="">
            <p>Music</p>
          </div>
          <div class="col-xs-12 col-md-6">
            <img src="./assets/images/games.jpg" class="img-responsive" alt="">
            <p>Games</p>
          </div>
        </div>
      </div>

      <div class="col-xs-12">
        <img src="./assets/images/footer.jpg" class="img-responsive" alt="">
      </div>

    </div>
  </div>
</div>

@endsection