@extends('navbar')
@section('content')
  
<div class="section-container">
    <div class="container">
      <div class="row">
        <div class="col-xs-12">
          <div class="section-container-spacer text-center">
            <h1 class="h2">03 : KONTAK GUA</h1>
          </div>
          
          <div class="row">
            <div class="col-md-10 col-md-offset-1">
               <form action="" class="reveal-content">
                  <div class="row">
                    <div class="col-md-7">
                      <div class="form-group">
                        <input type="email" class="form-control" id="email" placeholder="Email">
                      </div>
                      <div class="form-group">
                        <input type="text" class="form-control" id="subject" placeholder="Subject">
                      </div>
                      <div class="form-group">
                        <textarea class="form-control" rows="5" placeholder="Enter your message"></textarea>
                      </div>
                      <button type="submit" class="btn btn-default btn-lg">Send</button>
                    </div>
                    <div class="col-md-5 address-container">
                      <ul class="list-unstyled">
                        <li>
                          <span class="fa-icon">
                            <i class="fa fa-phone" aria-hidden="true"></i>
                          </span>
                          +62 896 0644 0409
                        </li>
                        <li>
                          <span class="fa-icon">
                            <i class="fa fa-at" aria-hidden="true"></i>
                          </span>
                          Mrbezaleel12@gmail.com
                        </li>
                        <li>
                          <span class="fa-icon">
                            <i class="fa fa fa-map-marker" aria-hidden="true"></i>
                          </span>
                          Gang Markisa, Markisa 1 Sambangan, Sukasada, 
                          Singaraja, Buleleng, Bali.
                        </li>
                      </ul>
                      <h3>Follow me on social networks</h3>
                      <a href="https://www.instagram.com/_yehudaaa/" title="" class="fa-icon">
                        <i class="fa fa-instagram"></i>
                      </a>
                      <a href="http://www.facebook.com/yuda.widia.33" title="" class="fa-icon">
                        <i class="fa fa-facebook"></i>
                      </a>
                      <a href="https://wa.me/6289606440409" title="" class="fa-icon">
                        <i class="fa fa-whatsapp"></i>
                      </a>
                    </div>
                  </div>
                </form>
            </div>
          </div>         
       </div>
      </div>
    </div>
  </div>
  
@endsection