@extends('navbar')
@section('content')
  
<div class="section-container">
    <div class="container">
      <div class="row">
        <div class="col-xs-12">
          <div class="section-container-spacer text-center">
            <h1 class="h2">02 : Tentang Gua</h1>
          </div>
          
          <div class="row">
            <div class="col-md-10 col-md-offset-1">
              <div class="row">
                <div class="col-xs-12 col-md-6">
                  <h3>BIODATA</h3>
                  <p>Hai welcome to my Blog, kenalkan nama gua Yehuda Bezaleel Widiapratama umur gua 19 tahun gua lahir di Batam, 02 Oktober 2021. Gua asli orang Batam yang dimana 
                      terkenal akan black marketnya, tapi sekarang gua sedang merantau kembali untuk melanjutkan Kuliah gua. </p>
                  <h3>CERITA PENGALAMAN</h3>
                  <p>Gua di Bali sejak baru beranjak ke Sekolah Menengah Atas lebih tepatnya di Sekolah SMA Negeri 4 Singaraja</h3>
                  <p>Dan sekarang gua harus melanjutkan perkuliahan gua di Universitas Pendidikan Ganesha a.k.a Undiksha. Banyak yang bertanya, Kenapa sekolah terlalu jauh? tidak di Batam saja? 
                      Pertanyaan yang sangat membosankan jika gua mendengar itu. Gua lebih memilih untuk berkuliah jauh ke Bali agar gua bisa lebih mandiri kedepannya, meskipun di Bali banyak keluarga,
                      tetapi gua lebih memilih untuk hidup sendiri dan ngekos. Gua dituntut harus bisa bersosialisasi agar gua memiliki banyak relasi dan memiliki banyak teman, dan bagi kalian yang ingin
                      untuk bersekolah jauh lebih baik siapkan mental, karena engkau harus survive untuk menjalani hari-hari mu sendiri tanpa bantuan orang tua.
                  </p>
                  <h3>QUOTES</h3>
                  <p> Janganlah kau mencari yang sempurna, Maka kau akan kehilangan yang terbaik. </p>
                </div>
                <div class="col-xs-12 col-md-6">
                  <img src="./assets/images/profile.jpg" class="img-responsive" alt="">
                </div>
              </div>
            </div>
          </div>          
       </div>
      </div>
    </div>
  </div>

@endsection