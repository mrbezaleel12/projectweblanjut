<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/index', [App\Http\Controllers\IndexController::class, 'indexcont']);

Route::get('/about', [App\Http\Controllers\AboutController::class, 'aboutcont']); 

Route::get('/contact', [App\Http\Controllers\ContactController::class, 'contactcont']);

Route::get('/hoby', [App\Http\Controllers\HobyController::class, 'hobycont']); 